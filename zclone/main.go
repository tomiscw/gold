// zclone project main.go
package main

import (
	"fmt"
	"os"
	"os/exec"

	"github.com/BurntSushi/toml"
)

type Config struct {
	Path  string
	Debug bool
}

var conf Config

func main() {

	if _, err := toml.DecodeFile("zclone.toml", &conf); err != nil {
		panic(err)
		return
	}

	app := "git"
	args := conf.Path + os.Args[1]

	if conf.Debug == true {
		fmt.Println("[Debug] Path: ", args)
	}

	if err := exec.Command(app, "clone", args).Run(); err != nil {
		panic(err)
	} else {
		fmt.Println("Done.")
	}

}
