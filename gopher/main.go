// main project main.go
package main

import (
	"fmt"
)

type FurInfo struct {
	name    string
	sex     string
	species string
	fanart  bool
}

var (
	tomis  FurInfo
	tango  FurInfo
	winter FurInfo
)

func print_info(fur FurInfo) {

	fmt.Println("Name: ", fur.name)
	fmt.Println("Sex: ", fur.sex)
	fmt.Println("Species: ", fur.species)
	fmt.Println("Fanart: ", fur.fanart)
	fmt.Println()
}

func main() {

	tomis := FurInfo{
		name:    "Tomis CW",
		sex:     "Cboy",
		species: "Fennec",
		fanart:  true}

	tango := FurInfo{
		name:    "Tango",
		sex:     "Male",
		species: "Zecoon",
		fanart:  true}

	winter := FurInfo{
		name:    "Winter Green",
		sex:     "Male",
		species: "German Shepherd",
		fanart:  false}

	print_info(tomis)
	print_info(tango)
	print_info(winter)
}
